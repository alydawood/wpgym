<?php
/*
* Template Name: Gallery
*/
get_header();
$thepost=array();
$gallery;
$gallery_images_ids;
$info=array();
while(have_posts() ){
    the_post();
    $gallery = get_post_gallery( get_the_ID(), false );
    $gallery_images_ids = explode(',', $gallery['ids']);
    $thepost['title']=get_the_title();
    foreach($gallery_images_ids as $id){
        $size = ($i === 3 || $i == 6) ? 'portrait' : 'square';
        $imageThumb = wp_get_attachment_image_src($id, 'square');
        $image = wp_get_attachment_image_src($id, 'large');
        $data=[
            'size'=>$size,
            'imageThumb'=>$imageThumb[0],
            'image'=>$image[0],
        ];
        array_push($info,$data);
    }
}
$thepost['gallery']=$info;
Timber::render('page-gallery.twig',$thepost);
wp_reset_postdata();
?>


<?php get_footer(); ?>
