<?php




 while(have_posts()){
    the_post();
    $img=get_the_post_thumbnail(get_the_ID(),'blog', array('class' => 'featured-image'));

    $data=[
        'title'=>get_the_title(),
        'img'=>$img,
       'start_time'=>get_field('start_time'),
         'end_time'=>get_field('end_time'),
        'class_days'=>get_field('class_days'),
        'content'=>get_the_content(),
        'post_type'=>get_post_type()
    ];

 }

 Timber::render('s.twig',$data);
 if( get_the_title() =='Contact us'){
   echo do_shortcode('[contact-form-7 id="80" title="Contact form 1"]');
    echo do_shortcode('[gymfitness_location]');
}

?>

