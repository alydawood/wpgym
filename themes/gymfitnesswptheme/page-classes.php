<?php get_header(); ?>

    <main class="container page section classes-index">
        <?php
            //gymfitness_classes_list(); udpated to below to use Twig
            $data['info']=gymfitness_classes_list_twig();
            Timber::render('page-classes.twig',$data);
        ?>
    </main>

<?php get_footer(); ?>
