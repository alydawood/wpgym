<?php get_header('front');

?>

<?php

while(have_posts()){
    $welcome=[
        'heading'=>get_field('front_welcome_heading'),
        'body'=>get_field('front_welcome_text')
    ];
    Timber::render('front_welcome.twig',$welcome);
    the_post();
}
?>

<section class="section-areas">
            <ul class="areas-container">
                <li class="area">
                    <?php
                        $area1 = get_field('area_1');
                        $image = wp_get_attachment_image_src( $area1['area_image'], 'mediumSize')[0];
                    ?>
                    <img src="<?php echo $image ?>" />
                    <p><?php echo $area1['area_name']; ?>
                </li>
                <li class="area">
                    <?php
                        $area2 = get_field('area_2');
                        $image = wp_get_attachment_image_src( $area2['area_image'], 'mediumSize')[0];
                    ?>
                    <img src="<?php echo $image ?>" />
                    <p><?php echo $area2['area_name']; ?>
                </li>

                <li class="area">
                    <?php
                        $area3 = get_field('area_3');
                        $image = wp_get_attachment_image_src( $area3['area_image'], 'mediumSize')[0];
                    ?>
                    <img src="<?php echo $image ?>" />
                    <p><?php echo $area3['area_name']; ?>
                </li>

                <li class="area">
                    <?php
                        $area4 = get_field('area_4');
                        $image = wp_get_attachment_image_src( $area4['area_image'], 'mediumSize')[0];
                    ?>
                    <img src="<?php echo $image ?>" />
                    <p><?php echo $area4['area_name']; ?>
                </li>
            </ul>
</section>

<section class="classes-homepage">
            <div class="container section">
                <h2 class="text-primary text-center">Our Classes</h2>

                <?php
                 $data['info']=gymfitness_classes_list_twig(4);
                 Timber::render('page-classes.twig',$data);
                //gymfitness_classes_list(4);
                ?>

                <div class="button-container">
                    <a class="button" href="<?php echo get_permalink( get_page_by_title('Classes') ); ?>">
                        View All Classes
                    </a>
                </div>
            </div>
        </section>
        <section class="instructors">
            <div class="container section">
                <h2 class="text-center">Our Instructors</h2>
                <p class="text-center">Professional intructors that will help you achieve your goals</p>

               <?php
                    $data['info']= gymfitness_instructors_list_twig();
                    Timber::render('front_instructors.twig',$data);

               ?>
            </div>
        </section>
<?php get_footer(); ?>
